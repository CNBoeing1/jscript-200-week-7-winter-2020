// TODO
let getValidity = function(inp){
	/*if (inp.getAttribute('id') === 'contact-kind'){
		return (inp.value === 'choose');
		
	}*/
	if(inp.getAttribute('name') === 'name'){
		return (inp.value.length < 3); 
	}
	else if(inp.getAttribute('name') === 'email'){
		let re = /\w+@\w+\.\w+/;
		return (!(re.test(inp.value)));
	}
	
	//else if(inp.getAttribute('name') === 'last-name'){
	//	return (inp.value.length < 3); 
	//}
	
};

let getHiddenValidity = function(){
	let hiddenDiv = document.getElementById('hidden_div');
	let initial = false;
	if (hiddenDiv.style.display === 'block'){

		if(document.getElementById('contact-reason').value === "opportunity"){
		
			let jobTitle = document.getElementById('jtitle');
			let cSite = document.getElementById('csite');
			
			let reg = /https?\:\/\/.+\..+/
			if(jobTitle.value.length === 0){
				initial = true;
				jobTitle.classList.add('invalid');
				console.log(jobTitle.validationMessage);
				document.getElementById('jtitlesmall').innerText = "Error: Must choose a Job Title.";
			}
			else{
			jobTitle.classList.remove('invalid');
			jobTitle.classList.add('valid');
			document.getElementById('jtitlesmall').innerText = '';
			}
			if( (cSite.value.length === 0)||(!(reg.test(cSite.value)))){
				initial = true;
				console.log("triggered stuff");
				cSite.classList.add('invalid');
				document.getElementById('csitesmall').innerText = "Error: Must enter a valid URL.";
			
			}
			else{
				cSite.classList.remove('invalid');
				cSite.classList.add('valid');
				document.getElementById('csitesmall').innerText = '';
			}
		}
	
	if(document.getElementById('contact-reason').value === "code"){
		const selectEl = document.getElementById("codelanguage");
		if(selectEl.value === 'choose'){
			initial = true;
			selectEl.classList.add('invalid');
			document.getElementById('hiddencodesall').innerText = "Error: Must select a option.";
			}
			else{
			selectEl.classList.remove('invalid');
			selectEl.classList.add('valid');
			document.getElementById('hiddencodesall').innerText = '';
			}
		}
	}
	return initial;
};

let showDiv = function showDiv(divId, element)
{
	document.getElementById(divId).style.display = 'block';

	if(element.value === "opportunity"){
		document.getElementById("hidden_code").style.display = 'none';
		document.getElementById('job-opportunities').style.display = 'block';
	}
	if(element.value === "code"){
		document.getElementById('hidden_code').style.display = 'block';
		document.getElementById("job-opportunities").style.display = 'none';
	}
}

const form = document.getElementById('connect-form');

form.addEventListener("submit", function(e){
	//e.preventDefault();
	let formIsValid = true;
	const inputs = document.getElementsByClassName('validate-input');
	
	Array.from(inputs).forEach(input => {
	
			const small = input.parentElement.querySelector('small');
			if (getValidity(input)){
				formIsValid = false;
					input.parentElement.classList.remove('valid');
				input.parentElement.classList.add('invalid');
				small.innerText = input.validationMessage;
			}
			else{
				small.innerText='';
				input.parentElement.classList.remove('invalid');
				input.parentElement.classList.add('valid');
			}
		});
		
	let ret = getHiddenValidity();
	console.log("returned" + ret);
	if(ret){
		formIsValid = false;
	}
		
		if(!formIsValid){
		e.preventDefault();
		console.log("Bad input");
		}
	
});
	
