/**
 * Car class
 * @constructor
 * @param {String} model
 */

//  Create an instance, accelerate twice, brake once, and console log the instance.toString()
class Car {
  constructor(brand) {  // Constructor
    this.currentSpeed = 0;
	this.model = brand;
	/*this.accelerate = function () {
            this.currentSpeed++;
			}
	this.brake = function () {
            this.currentSpeed--;
			}
	this.toString = function() {
		return this.model + " running " + this.currentSpeed;*/
	}
	accelerate() {
            this.currentSpeed++;
			}
	brake() {
            this.currentSpeed--;
			}
	toString() {
		return this.model + " running " + this.currentSpeed;
  }
}
const mycar = new Car("Ford");
mycar.accelerate();
mycar.accelerate();
mycar.brake();
console.log(mycar.toString());
/**
 * ElectricCar class
 * @constructor
 * @param {String} model
 */

//  Create an instance, accelerate twice, brake once, and console log the instance.toString()
class ElectricCar extends Car{
	constructor(brand){
		super(brand);
		this.motor = 'electric';	
		this.toString = function(){
		return "Electric running at " + this.currentSpeed;}
		
	
	
	}
	accelerate(){
			super.accelerate();
			super.accelerate();
		}
	
	
}

const tesla = new ElectricCar("Tesla");
tesla.accelerate();
tesla.accelerate();
tesla.brake();
console.log(tesla.toString());