// TODO
let getValidity = function(inp){
	/*if (inp.getAttribute('id') === 'contact-kind'){
		return (inp.value === 'choose');
		
	}*/
	if(inp.getAttribute('name') === 'first-name'){
		return (inp.value.length < 3); 
	}
	
	else if(inp.getAttribute('name') === 'last-name'){
		return (inp.value.length < 3); 
	}
	else if(inp.getAttribute('name') === 'email'){
		let re = /\w+@\w+\.\w+/;
		return (!(re.test(inp.value)));
	}
};


const form = document.getElementById('connect-form');

form.addEventListener("submit", function(e){
	//e.preventDefault();
	let formIsValid = true;
	const inputs = document.getElementsByClassName('validate-input');
	
	Array.from(inputs).forEach(input => {
	
			const small = input.parentElement.querySelector('small');
			if (getValidity(input)){
				formIsValid = false;
					input.parentElement.classList.remove('valid');
				input.parentElement.classList.add('invalid');
				small.innerText = input.validationMessage;
			}
			else{
				small.innerText='';
				input.parentElement.classList.remove('invalid');
				input.parentElement.classList.add('valid');
			}
		});
		
		if(!formIsValid){
		e.preventDefault();
		console.log("Bad input");
		}
	
});
	
